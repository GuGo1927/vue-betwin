import Vue from 'vue'
import Router from 'vue-router'
import SignIn from '../components/admin/SignIn'
import AuthTwoFactor from '../components/admin/AuthTwoFactor'
import AuthTwoFactorCode from '../components/admin/AuthTwoFactorCode'
import TrustedIps from '../components/dashboard/settings/TrustedIps'
import Affilates from '../components/dashboard/affilates/Affilates'
import AffilatesList from '../components/dashboard/affilates/Affilates.list'
import AffilatesReview from '../components/dashboard/affilates/Affilates.review'
import AffilatesGroups from '../components/dashboard/affilates/groups/Groups'
import GroupsInfo from '../components/dashboard/affilates/groups/Groups.info'
import Settings from '../components/dashboard/settings/Settings'
import { store } from '@/stores/store'
import * as AdminServiceAction from '@/stores/modules/admin/types';

Vue.use(Router)


let router = new Router({
  routes: [
    {
      path: '/',
      name: 'sign-in',
      component: SignIn,
      meta: { closeForAuth: true }
    },
    {
      path: '/auth-two-factor',
      name: 'AuthTwoFactor',
      component: AuthTwoFactor
    },
    {
      path: '/auth-two-factor-code',
      name: 'AuthTwoFactorCode',
      component: AuthTwoFactorCode
    },
    {
      path: '/settings',
      component: Settings,
      name: "Settings",
      redirect: {path: '/settings/ips'},
      children: [
        {
          path: '/settings/ips',
          name: "Trusted Ip's",
          component: TrustedIps,
        }
      ],
      meta: { requiresAuth: true },
    },
    {
      path: '/affilates',
      component: Affilates,
      name: "Affilates",
      redirect: {path: '/affilates/list'},
      children: [
        {
          path: '/affilates/list',
          name: "Affilate List",
          component: AffilatesList,
        },
        {
          path: '/affilates/review',
          name: "Affilate Review",
          component: AffilatesReview,
        },
        {
          path: '/affilates/requests',
          name: "Affilate Requests",
          component: AffilatesReview,
        },
        {
          path: '/affilates/groups',
          name: "Affilate Groups",
          component: AffilatesGroups,
          redirect: {path: '/affilates/groups/info'},
          children: [
            {
              path: '/affilates/groups/info',
              name: "Groups Information",
              component: GroupsInfo,
            },
            {
              path: '/affilates/groups/sub',
              name: "Sub Affilates",
              component: AffilatesList,
            },
            {
              path: '/affilates/groups/offers',
              name: "Offers",
              component: AffilatesList,
            },
            {
              path: '/affilates/groups/bonuses',
              name: "Bonuses",
              component: AffilatesList,
            },
          ],
        },
      ],
      meta: { requiresAuth: true },
    },
  ]
});

router.beforeEach((to, from, next) => {
  const token = Vue.cookie.get ('token');

  let ValidateRouter = () => {
    if (to.matched.some(record => record.meta.requiresAuth)) {

      if (store.state.admin.isAuthenticated === false) {
        next({
          path: '/',
          query: { redirect: to.fullPath }
        })
      } else {
        next()
      }
    }  else if (to.matched.some(record => record.meta.closeForAuth)) {
      if (store.state.admin.isAuthenticated != false  && store.state.admin.isAuthenticated != undefined) {
        next({
          path: '/settings/ips',
          query: { redirect: to.fullPath }
        })
      } else {
        next()
      }
    } else {
      next()
    }

    store.commit('Complete')
  };

  if(token != undefined && token != "")
  {
      store.dispatch('admin/'+AdminServiceAction.LOGIN_FROM_COOKIE,  JSON.parse(Vue.cookie.get ('user')));
      ValidateRouter();
  } else {
    ValidateRouter();
  }

});

export default router
