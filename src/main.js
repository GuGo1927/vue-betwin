// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import { store } from './stores/store';
import vuexI18n from 'vuex-i18n';
require('./libs/setup.plugins');

import VeeValidate from 'vee-validate';
VeeValidate.Validator.extend('ipaddress', {
  validate (value) {
    return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(value)
  },
  getMessage (field) {
    return `The IP must correct ip address`;
  }
});
Vue.use(VeeValidate, {fieldsBagName: 'formFields'});

import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

import VueCookie from 'vue-cookie';
Vue.use(VueCookie);

Vue.config.productionTip = false

import LangEn from './translations/en';
import LangRu from './translations/ru';

Vue.use(vuexI18n.plugin, store);

Vue.i18n.add('en', LangEn);
Vue.i18n.add('ru', LangRu);

Vue.i18n.set('en');


Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store
})
