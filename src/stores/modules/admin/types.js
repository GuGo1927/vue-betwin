/** Authorization action **/
export const A_LOGIN = "A_LOGIN";

/** Authorization fail event **/
export const M_LOGIN_FAIL = "M_LOGIN_FAIL";

export const LOGIN_FROM_COOKIE = "LOGIN_FROM_COOKIE";

export const A_PROFILE = "A_PROFILE";

export const A_TOTP_AUTH = "A_TOTP_AUTH";

export const A_CHANGE_PASSWORD = "A_CHANGE_PASSWORD";

