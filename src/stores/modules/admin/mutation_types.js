/** Authorization success event **/
export const M_LOGIN_SUCCESS = "M_LOGIN_SUCCESS";

/** Authorization fail event **/
export const M_LOGIN_FAIL = "M_LOGIN_FAIL";

export const LOGIN_INIT_DATA = "LOGIN_INIT_DATA";

export const M_LOGIN_ASK_TOTP = "M_LOGIN_ASK_TOTP";

export const M_PROFILE_SUCCESS = "M_PROFILE_SUCCESS";

export const M_TOTP_SUCCESS = "M_TOTP";

export const M_TOTP_FAIL = "M_TOTP_FAIL";

export const M_CHANGE_PASSWORD_SUCCESS = "M_CHANGE_PASSWORD_SUCCESS";

export const M_CHANGE_PASSWORD_FAIL = "M_CHANGE_PASSWORD_FAIL";

