import * as mutation_types from './mutation_types';

export default {

  /**
   * Authorization success Handler
   * @param state
   * @param payload
   */
  [mutation_types.M_LOGIN_SUCCESS](state, payload) {
    state.isAuthenticated = true;
    state.token = payload.token;
    state.token_expired = payload.expired;
    state.user = payload.user;
    state.isValidateToken = true;
  },

  /**
   * Authorization fail Handler
   * @param state
   * @param payload
   */
  [mutation_types.M_LOGIN_FAIL]( state, payload )
  {
    state.isAuthenticated = false;
    state.user = null;
    state.statusCode = true
  },

  [mutation_types.M_LOGIN_ASK_TOTP]( state, payload )
  {
    state.askTotp = true;
  },

  [mutation_types.M_TOTP_SUCCESS]( state, payload )
  {
    state.askTotp = true;
  },
  [mutation_types.M_TOTP_FAIL]( state, payload )
  {
    state.askTotp = false;
  },

  [mutation_types.M_PROFILE_SUCCESS]( state, payload )
  {
    state.profile = payload.data.payload
  },

  [mutation_types.LOGIN_INIT_DATA]( state, payload )
  {
    state.isAuthenticated = true;
    state.token = payload.id;
    // state.token_expired = payload.expired;
    state.user = payload;
    state.isValidateToken = true;
  },

  [mutation_types.M_CHANGE_PASSWORD_SUCCESS]( state, payload )
  {
  },
  [mutation_types.M_CHANGE_PASSWORD_FAIL]( state, payload )
  {
  },

}
