import Vue from 'vue';

export default (token) => {
  var o = { token: token, trust: true };
  return Vue.Api.Post("https://api.bwaff.info/v1/admin/auth/totp", o);

};
