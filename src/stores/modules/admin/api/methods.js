import API_ChangeLangRequest from './change.language';
import API_SignInRequest from './signin';
import API_GetProfile from './getprofile';
import API_2FaAuth from './2fa.auth';
import API_ChangePassword from './changepassword';

export const ChangeLangRequest  = API_ChangeLangRequest;
export const SignInRequest          = API_SignInRequest;
export const GetProfileRequest          = API_GetProfile;
export const TotpAuth          = API_2FaAuth;
export const ChangePasswordRequest          = API_ChangePassword;
