import Vue from 'vue';

export default () => {

  return Vue.Api.Get("https://api.bwaff.info/v1/admin/profile");

};
