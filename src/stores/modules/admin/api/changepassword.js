import Vue from 'vue';

export default (old_password, new_password) => {
  var o = { old_password: old_password, new_password: new_password };

  return Vue.Api.Post("https://api.bwaff.info/v1/admin/profile/update-password", o);

};
