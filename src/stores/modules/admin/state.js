export default {

  isAuthenticated: false,
  user: null,
  token: null,
  token_expired: 0,
  isValidateToken: false,
  statusCode: false,
  askTotp: false,
  profile: {},

  connection: {
    '2fa': null,
  },

}
