import * as types from './types';
import * as mutation_types from './mutation_types';
import {
  SignInRequest,
  ChangeLangRequest,
  GetProfileRequest,
  TotpAuth,
  ChangePasswordRequest
} from './api/methods';

export default {

  [types.A_LOGIN] ( context, payload) {
    SignInRequest (payload.email, payload.password, payload.remember).then (
      ( response ) => {
        if(response.status == 200 && response.data) {
          if(response.data.otp_auth_url == undefined&& response.data.otp_qr_code_url == undefined) {
            context.commit (mutation_types.M_LOGIN_SUCCESS, {
              token: response.data.payload.id,
              user: response.data.payload,
              isAuthenticated: true
            });
          } else {
            context.commit (mutation_types.M_LOGIN_ASK_TOTP, {
              token: response.data.payload.id,
              user: response.data.payload,
              isAuthenticated: true,
              askTotp: true
          });
          }
        } else {
          context.commit (mutation_types.M_LOGIN_FAIL, {
            message: "Email or Password incorrect",
            error: {}
          });
        }
      }, ( error ) => {
        context.commit (mutation_types.M_LOGIN_FAIL, { error: error })
      }
    ).catch((error) => {
      context.commit (mutation_types.M_LOGIN_FAIL, { error: error })
    })
  },

  /**
   * Set Auth Data From Cookies
   * @param context
   * @param payload
   */
  [types.LOGIN_FROM_COOKIE] (context, payload){
    context.commit (mutation_types.LOGIN_INIT_DATA, payload);
  },

  [types.A_TOTP_AUTH] (context, payload){
    TotpAuth (payload.token).then (
      ( response ) => {
        console.log()
        if(response.data.payload.result == false) {
          context.commit (mutation_types.M_TOTP_FAIL, payload);
        } else {
          context.commit (mutation_types.M_TOTP_SUCCESS, payload);
        }
      }, ( error ) => {
        context.commit (mutation_types.M_TOTP_FAIL, payload);
      }
    ).catch(( error ) => {
      context.commit (mutation_types.M_TOTP_FAIL, payload);
    })
  },

  [types.A_PROFILE] (context, payload){
    GetProfileRequest ().then (
      ( response ) => {
        context.commit (mutation_types.M_PROFILE_SUCCESS, response);
      }, ( error ) => {
        if(error.status == 423) {
          context.commit (mutation_types.M_LOGIN_ASK_TOTP, payload);
        }
      }
    ).catch(( error ) => {
      console.log(error)
    })
  },

  [types.A_CHANGE_PASSWORD] (context, payload) {
    ChangePasswordRequest (payload.old_password, payload.new_password).then(
      ( response ) => {
        context.commit (mutation_types.M_CHANGE_PASSWORD_SUCCESS, payload);
      }, ( error ) => {
        context.commit (mutation_types.M_CHANGE_PASSWORD_FAIL, payload);
      }
    ).catch((error) => {
      context.commit (mutation_types.M_CHANGE_PASSWORD_FAIL, payload);
    })
  }


}
