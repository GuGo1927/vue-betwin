
export default {

  getTokenTotalCount: ( state, getters ) => (searchFilter) =>{
    let result = state.affilates_lists;
    return result.filter(item => {
      return item.email.toString().includes(searchFilter.toString())
    }).length;
  },

}
