import * as mutation_types from './types.mutation';

export default {

  [mutation_types.M_ADD_NEW_IP_COMPLETE] ( state, payload ) {
    state.ips.push(payload.payload)
  }
};

