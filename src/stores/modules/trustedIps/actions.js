import * as types from './types';
import * as mutation_types from './types.mutation';

export default {
  [types.ADD_NEW_IP] ( contex, payload) {
    contex.commit (mutation_types.M_ADD_NEW_IP_COMPLETE, {payload})
  }
}
