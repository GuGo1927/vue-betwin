export default {
  ips: [
    { source: '12.212.412', description: "John" },
    { source: '32.123.412', description: "Jane" },
    { source: '142.123.412', description: "Susan"},
    { source: '153.123.412', description: "Chris"},
    { source: '1223.123.412', description: "Dan"},
    { source: '43.2342.43', description: "John"},
    { source: '55.123.412', description: "Jane"},
    { source: '23.123.412', description: "Susan"},
    { source: '122.123.412', description: "Chris"},
    { source: '532.123.412', description: "Dan"},
    { source: '734.123.412', description: "Dan"},
    { source: '452.123.412', description: "Dan"},
    { source: '468.123.412', description: "Dan"},
    { source: '783.123.412', description: "Dan"},
    { source: '674.123.412', description: "Dan"},
    { source: '235.123.412', description: "Chris"},
    { source: '468.123.412', description: "Dan"},
  ]

}
