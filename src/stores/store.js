import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'
import AdminModules from './modules/admin/store'
import TrustedIps from './modules/trustedIps/store'
import AffilatesList from './modules/affilates/store'

Vue.use(Vuex);

export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    admin: AdminModules,
    ips:TrustedIps,
    affilatesList:AffilatesList
  },
});
