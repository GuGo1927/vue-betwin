import Vue from 'vue'
// import Validator from './plugins/vue.form.validator'
import Api from './plugins/api.adapter'
import Storage from 'vue-web-storage'

// import TokenValidator from './plugins/token.validator';

import VTooltip from 'v-tooltip'
Vue.use(Api)

Vue.use(VTooltip)
// Vue.use(Validator);
Vue.use(Storage)
// Vue.use(TokenValidator, { service_url: process.env.USER_SERVICE_URL });

// require('./setup.ui')
// require('./setup.resource')
// require('./plugins/vue.directives')
